#include <SDL.h>
#include <SDL_ttf.h>

#include<iostream>
#include<ctime>
#include<fstream>
#include <cmath>

#include "config_sdl.h"
#include "Header.h"

#include <iostream>
#define _CRTDBG_MAP_ALLOC
#define _DEBUG
#include <Windows.h>
#include <stdlib.h>
#include <crtdbg.h>

#define _USE_MATH_DEFINES // pour avoir pi facilement ^^

using namespace std;

/*=======================================
			Jouer sur une map
=========================================*/



/*==========INITIALISATION=============*/


//Rappel : x = Abscisse et y = Ordonnée

// initialise un point quelconque
void init_point_ligne(Point& p, int x, int y) {
	p.x = x;
	p.y = y;
}

//initialise la ligne
void init_ligne(Ligne& l, int Xdepart, int Ydepart, int Xarrive, int Yarrive, int nbrPoints) {

	init_point_ligne(l.depart, Xdepart, Ydepart);
	init_point_ligne(l.arrivee, Xarrive, Yarrive);

	l.nbrPoints = nbrPoints;

	init_point_ligne(l.ligne[0], l.depart.x, l.depart.y);

	for (int k = 1; k < 100; k++) {
		init_point_ligne(l.ligne[k], -1, -1);
	}
}

//initialise un obstacle
void init_obstacle(Obstacle& obstacle, Forme forme, int x, int y, int rayonX, int rayonY, SDL_Color color)
{
	obstacle.forme = forme;
	obstacle.x = x;
	obstacle.y = y;
	obstacle.rayonX = rayonX;
	obstacle.rayonY = rayonY;
	obstacle.color = color;
}





//Initialisation d'une map
void init_map(Map& map, const char nomMap[50], const char fond[10], Ligne ligne, Obstacle TABobstacle[150], int nbrObstacles)
{
	strcpy_s(map.nomMap, 49, nomMap);
	strcpy_s(map.fond, 9, fond);
	map.ligne = ligne;
	map.nbrObstacles = nbrObstacles;


	for (int i = 0; i < nbrObstacles; i++) {
		init_obstacle(map.TABobstacle[i], TABobstacle[i].forme, TABobstacle[i].x, TABobstacle[i].y, TABobstacle[i].rayonX, TABobstacle[i].rayonY, TABobstacle[i].color);
	}
}



void init_bouton(Bouton& bouton, SDL_Renderer* rendu,  int x, int y, int w, int h, const char nom[], STATUS status, const char texture1[], const char texture2[]) {

	bouton.x = x;
	bouton.y = y;
	bouton.w = w;
	bouton.h = h;

	strcpy_s(bouton.nom, 49, nom);

	bouton.status = status;

	bouton.imageUnPress = loadImage(rendu, texture1);
	bouton.imagePress = loadImage(rendu, texture2);
}



/*===============AJOUT=================*/

//on se rappelle que dans la suite, x et y vont correspondre à des clique de souris
//au début se n'est pas très grave mais il faudra bien ce dire que l'utilisateur peut cliquer dans un carre qui coresspond au coin
//il a donc environ 5 pixel dans chaque direction autour de la coordonnée du point pour que sont clique soit valable ^^




//ajoute un obstacle sur la map
void ajoute_obstacle(Map& m, Forme f, int x, int y, int rayonX, int rayonY, SDL_Color color)
{
	init_obstacle(m.TABobstacle[m.nbrObstacles], f, x, y, rayonX, rayonY, color);
	m.nbrObstacles++;
}

/*===============POINTS=================*/

//return l'indice du point cliquer ou -1 si il ny en a pas 
int clique_points(Ligne& l, int x, int y) {

	for (int i = 1; i < l.nbrPoints; i++) {
		if (x > (l.ligne[i].x - 7.5) && x < (l.ligne[i].x + 7.5) && y >(l.ligne[i].y - 7.5) && y < (l.ligne[i].y + 7.5)) {

			return i;
		}
	}
	return -1;
}

//ajoute un point sur la ligne, augmente aussi Ipoints
void ajoute_point(Ligne& l, int x, int y)
{
	l.ligne[l.nbrPoints].x = x;
	l.ligne[l.nbrPoints].y = y;
	l.nbrPoints++;
}

void supprime_point(Ligne& l, int x, int y) {
	int cpt = 0;

	int i = clique_points(l, x, y);

	if (i != -1) {
		for (int k = i; k < l.nbrPoints; k++) {
			l.ligne[k].x = -1;
			l.ligne[k].y = -1;
			cpt++;
		}
	}

	l.nbrPoints = l.nbrPoints - cpt;
}





//verifie si la ligne est bien placé et ne cogne pas les obstacle
bool verif_ligne(SDL_Renderer * rendu, Map & m, Robot & r) {

	// on va regarder chaque point de la ligne (100 points sur la ligne  en tout cas)
	// forme : (x2 - x1) / 100 + x1


	bool contact = true;

	for (int j = 0; j < m.ligne.nbrPoints - 1; j++) {
		float x = m.ligne.ligne[j].x;
		float y = m.ligne.ligne[j].y;
		for (int k = 0; k < 100; k++) {
			x += (m.ligne.ligne[j + 1].x - m.ligne.ligne[j].x) / 100.0;
			y += (m.ligne.ligne[j + 1].y - m.ligne.ligne[j].y) / 100.0;
			// permet de tester chaque points sur la ligne


			float xGauche = x - r.taille / 2;
			float yHaute = y - r.taille / 2;
			float xDroite = x + r.taille / 2;
			float yBasse = y + r.taille / 2;


			bool verif = true;

			for (int i = 0; i < m.nbrObstacles; i++) {
				switch (m.TABobstacle[i].forme) {
				case RECTANGLE:
					if ((m.TABobstacle[i].x < xGauche && m.TABobstacle[i].x + m.TABobstacle[i].rayonX> xGauche
						|| m.TABobstacle[i].x > xGauche && m.TABobstacle[i].x < xDroite)
						&& (m.TABobstacle[i].y < yHaute && m.TABobstacle[i].y + m.TABobstacle[i].rayonY > yBasse
							|| m.TABobstacle[i].y > yHaute && m.TABobstacle[i].y < yBasse)) {

						verif = false;
						contact = false;
					}
					break;
				case CERCLE:
					for (double dy = 1; dy <= m.TABobstacle[i].rayonX; dy += 1.0)
					{
						double dx = floor(sqrt((2.0 * m.TABobstacle[i].rayonX * dy) - (dy * dy)));

						if ((m.TABobstacle[i].x - dx < xGauche && m.TABobstacle[i].x + dx > xGauche
							|| m.TABobstacle[i].x - dx > xGauche && m.TABobstacle[i].x - dx < xDroite)
							&& (m.TABobstacle[i].y - m.TABobstacle[i].rayonY + dy > yHaute
								&& m.TABobstacle[i].y - m.TABobstacle[i].rayonY + dy < yBasse
								|| m.TABobstacle[i].y + m.TABobstacle[i].rayonY - dy < yBasse
								&& m.TABobstacle[i].y + m.TABobstacle[i].rayonY - dy > yHaute)) {

							verif = false;
							contact = false;
						}
					}

					break;
				}
			}

			if (verif == true)
				SDL_SetRenderDrawColor(rendu, 166, 214, 8, 255);
			else {
				contact = false;
				SDL_SetRenderDrawColor(rendu, 252, 61, 49, 255);
			}


			for (double dy = 1; dy <= r.taille; dy += 1.0)
			{
				double dx = floor(sqrt((2.0 * r.taille * dy) - (dy * dy)));
				SDL_RenderDrawLine(rendu, x - dx, y + dy - r.taille, x + dx, y + dy - r.taille);
				SDL_RenderDrawLine(rendu, x - dx, y - dy + r.taille, x + dx, y - dy + r.taille);
			}
			SDL_RenderPresent(rendu);
		}
	}

	return contact;
}


/*=======================================
				Interface
=========================================*/


void affichageMenuSelection(SDL_Renderer* rendu)
{
	SDL_Texture* texture_menuSelection = loadImage(rendu, "SPRITES/menuSelection.jpg");
	SDL_Texture* texture_boutonMapOcean = loadImage(rendu, "SPRITES/boutonMapOcean.png");

	SDL_Rect menuSelection;
	menuSelection.x = 0;
	menuSelection.y = 0;
	menuSelection.w = LARGEUR;
	menuSelection.h = HAUTEUR;
	SDL_RenderCopy(rendu, texture_menuSelection, NULL, &menuSelection);

	SDL_Rect boutonMapOcean;
	boutonMapOcean.x = 70;
	boutonMapOcean.y = 20;
	boutonMapOcean.w = 640 / 3;
	boutonMapOcean.h = 320 / 3;
	SDL_RenderCopy(rendu, texture_boutonMapOcean, NULL, &boutonMapOcean);

	SDL_RenderPresent(rendu);
	SDL_DestroyTexture(texture_menuSelection);
	SDL_DestroyTexture(texture_boutonMapOcean);
}


void affichagePoint(Ligne& l, SDL_Renderer* rendu)
{
	for (int i = 0; i < 100; i++)
	{
		if (l.ligne[i].x != -1 && l.ligne[i].y != -1)
		{
			SDL_Rect point;

			point.x = (l.ligne[i].x) - 7, 5;
			point.y = (l.ligne[i].y) - 7, 5;
			point.w = 15;
			point.h = 15;
			SDL_SetRenderDrawColor(rendu, 255, 114, 0, 100);
			SDL_RenderFillRect(rendu, &point);
			SDL_SetRenderDrawColor(rendu, 210, 16, 52, 255);
			SDL_RenderDrawRect(rendu, &point);
		}
	}
}

void affichageLigne(Ligne& l, SDL_Renderer* rendu)
{
	for (int i = 0; i < 99; i++)
	{
		if (l.ligne[i].x != -1 && l.ligne[i].y != -1 && l.ligne[i + 1].x != -1 && l.ligne[i + 1].y != -1)
		{
			SDL_SetRenderDrawColor(rendu, 0, 0, 0, 255);
			SDL_RenderDrawLine(rendu, l.ligne[i].x, l.ligne[i].y, l.ligne[i + 1].x, l.ligne[i + 1].y); //x1,y1,x2,y2
		}
	}
}

void affichageObstacle(SDL_Renderer* rendu, Map& m) {

	if (strcmp(m.fond, "Ocean") == 0)
	{
		SDL_Texture* texture_obstaclePoissons = loadImage(rendu, "SPRITES/obstaclePoissons.png");
		SDL_Texture* texture_obstaclePlongeur = loadImage(rendu, "SPRITES/obstaclePlongeur.png");
		SDL_Texture* texture_obstacleRequin = loadImage(rendu, "SPRITES/obstacleRequin.png");
		SDL_Texture* texture_obstacleBaleine = loadImage(rendu, "SPRITES/obstacleBaleine.png");

		SDL_Rect poissons;
		poissons.x = 80;
		poissons.y = 100;
		poissons.w = 500 / 1.8;
		poissons.h = 303 / 1.8;
		SDL_RenderCopy(rendu, texture_obstaclePoissons, NULL, &poissons);

		SDL_Rect plongeur;
		plongeur.x = 500;
		plongeur.y = 0;
		plongeur.w = 510 / 3;
		plongeur.h = 600 / 3;
		SDL_RenderCopy(rendu, texture_obstaclePlongeur, NULL, &plongeur);

		SDL_Rect requin;
		requin.x = 250;
		requin.y = 455;
		requin.w = 921 / 2.2;
		requin.h = 584 / 2.2;
		SDL_RenderCopy(rendu, texture_obstacleRequin, NULL, &requin);

		SDL_Rect baleine;
		baleine.x = 360;
		baleine.y = 240;
		baleine.w = 1440 / 3.3;
		baleine.h = 552 / 3.3;
		SDL_RenderCopy(rendu, texture_obstacleBaleine, NULL, &baleine);

		SDL_RenderPresent(rendu);
		SDL_DestroyTexture(texture_obstaclePoissons);
		SDL_DestroyTexture(texture_obstaclePlongeur);
		SDL_DestroyTexture(texture_obstacleRequin);
		SDL_DestroyTexture(texture_obstacleBaleine);
	}
	else if (strcmp(m.fond, "Foret") == 0)
	{
		SDL_Texture* texture_sanglier = loadImage(rendu, "SPRITES/obstacleSanglier.png");
		SDL_Texture* texture_oiseau = loadImage(rendu, "SPRITES/obstacleOiseau.png");
		SDL_Texture* texture_ecureuil = loadImage(rendu, "SPRITES/obstacleEcureuil.png");
		SDL_Texture* texture_cerf = loadImage(rendu, "SPRITES/obstacleCerf.png");

		SDL_Rect sanglier;
		sanglier.x = 600;
		sanglier.y = 480;
		sanglier.w = 512 / 2;
		sanglier.h = 436 / 2;
		SDL_RenderCopy(rendu, texture_sanglier, NULL, &sanglier);

		SDL_Rect oiseau;
		oiseau.x = 575;
		oiseau.y = 50;
		oiseau.w = 500 / 3;
		oiseau.h = 463 / 3;
		SDL_RenderCopy(rendu, texture_oiseau, NULL, &oiseau);

		SDL_Rect ecureuil;
		ecureuil.x = 100;
		ecureuil.y = 450;
		ecureuil.w = 883 / 6;
		ecureuil.h = 941 / 6;
		SDL_RenderCopy(rendu, texture_ecureuil, NULL, &ecureuil);

		SDL_Rect cerf;
		cerf.x = 350;
		cerf.y = 250;
		cerf.w = 1024 / 3.5;
		cerf.h = 1024 / 3.5;
		SDL_RenderCopy(rendu, texture_cerf, NULL, &cerf);

		SDL_RenderPresent(rendu);
		SDL_DestroyTexture(texture_sanglier);
		SDL_DestroyTexture(texture_oiseau);
		SDL_DestroyTexture(texture_cerf);

	}
	else {
		for (int k = 0; k < m.nbrObstacles; k++) {

			switch (m.TABobstacle[k].forme) {
			case RECTANGLE:

				SDL_Rect RECT;

				RECT.x = m.TABobstacle[k].x;
				RECT.y = m.TABobstacle[k].y;
				RECT.w = m.TABobstacle[k].rayonX;
				RECT.h = m.TABobstacle[k].rayonY;
				SDL_SetRenderDrawColor(rendu, m.TABobstacle[k].color.r, m.TABobstacle[k].color.g, m.TABobstacle[k].color.b, m.TABobstacle[k].color.a);
				SDL_RenderFillRect(rendu, &RECT);

				break;
			case CERCLE:

				for (double dy = 1; dy <= m.TABobstacle[k].rayonX; dy += 1.0)
				{
					double dx = floor(sqrt((2.0 * m.TABobstacle[k].rayonX * dy) - (dy * dy)));
					int x = m.TABobstacle[k].x - dx;
					SDL_SetRenderDrawColor(rendu, m.TABobstacle[k].color.r, m.TABobstacle[k].color.g, m.TABobstacle[k].color.b, m.TABobstacle[k].color.a);
					SDL_RenderDrawLine(rendu, m.TABobstacle[k].x - dx, m.TABobstacle[k].y + dy - m.TABobstacle[k].rayonX, m.TABobstacle[k].x + dx, m.TABobstacle[k].y + dy - m.TABobstacle[k].rayonX);
					SDL_RenderDrawLine(rendu, m.TABobstacle[k].x - dx, m.TABobstacle[k].y - dy + m.TABobstacle[k].rayonX, m.TABobstacle[k].x + dx, m.TABobstacle[k].y - dy + m.TABobstacle[k].rayonX);
				}

				break;
			}


		}

	}
}

//affichage des points de départ et d'arriver 
void affichageDepart_Arriver(SDL_Renderer* rendu, Map& m) {

	SDL_Rect pointDepart;
	pointDepart.x = m.ligne.depart.x - DEPARTARRIVEE / 2;
	pointDepart.y = m.ligne.depart.y - DEPARTARRIVEE / 2;
	pointDepart.w = DEPARTARRIVEE;
	pointDepart.h = DEPARTARRIVEE;
	SDL_SetRenderDrawColor(rendu, 0, 255, 0, 255);
	SDL_RenderFillRect(rendu, &pointDepart);

	SDL_Rect pointArrivee;
	pointArrivee.x = m.ligne.arrivee.x - DEPARTARRIVEE / 2;
	pointArrivee.y = m.ligne.arrivee.y - DEPARTARRIVEE / 2;
	pointArrivee.w = DEPARTARRIVEE;
	pointArrivee.h = DEPARTARRIVEE;
	SDL_SetRenderDrawColor(rendu, 255, 0, 0, 255);
	SDL_RenderFillRect(rendu, &pointArrivee);

}

//affichage des fonds
void affichageFond(SDL_Renderer* rendu, Map& m) {
	SDL_Texture* texture = NULL;
		
	SDL_Rect fond;
	fond.x = 0;
	fond.y = 0;
	fond.w = WZONEGAUCHE;
	fond.h = HAUTEUR;

	switch (StatusG) {
	case MENU_PRINC:
		texture = loadImage(rendu, "SPRITES/homepage.jpeg");
		fond.w = LARGEUR;
		fond.h = HAUTEUR;
		SDL_RenderCopy(rendu, texture, NULL, &fond);
		SDL_DestroyTexture(texture);
		break;
	case CREATION:
		SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255);

		SDL_RenderFillRect(rendu, &fond);
		break;
	case CHOIX_MAPS:
		texture = loadImage(rendu, "SPRITES/menuSelection.jpg");
		fond.w = LARGEUR;
		fond.h = HAUTEUR;
		SDL_RenderCopy(rendu, texture, NULL, &fond);
		SDL_DestroyTexture(texture);
		break;
	case GAME:
		if (strcmp(m.fond, "Ocean") == 0) {
			texture = loadImage(rendu, "SPRITES/mapOcean.jpg");
			SDL_RenderCopy(rendu, texture, NULL, &fond);
			SDL_DestroyTexture(texture);
		}
		else if (strcmp(m.fond, "Foret") == 0) {
			texture = loadImage(rendu, "SPRITES/mapForet.jpeg");
			SDL_RenderCopy(rendu, texture, NULL, &fond);
			SDL_DestroyTexture(texture);
		}
		else 
		{
			SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255);

			SDL_RenderFillRect(rendu, &fond);
		}
		break;
	}
}

//affichage des boutons
void affichageBouton(SDL_Renderer* rendu) {

	for (int k = 0; k < nbrBoutons; k++) {

		SDL_Rect B;

		switch (StatusG) {
		case MENU_PRINC:
			if (strcmp(boutons[k].nom, "JOUER") == 0) {
				B = {boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "CREER_MAP") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}

			break;
		case GAME:
			if (strcmp(boutons[k].nom, "Ajouter") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "ZoneEcriture") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
				SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255);
				SDL_Rect Barre = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h};
				SDL_RenderFillRect(rendu, &Barre);
			}
			if (strcmp(boutons[k].nom, "Modifier") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "Supprim") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "Regle") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "Rapport") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "Annuler") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "Valider") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "RETOUR") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "VALID") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}

			break;
		case CREATION:
			
			if (strcmp(boutons[k].nom, "Rectangle") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "Cercle") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "RETOUR") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			if (strcmp(boutons[k].nom, "SAVE") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			break;
		case CHOIX_MAPS:
			if (strcmp(boutons[k].nom, "RETOUR") == 0) {
				B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
				SDL_RenderCopy(rendu, boutons[k].imageUnPress, NULL, &B);
			}
			break;
		}

		if (boutons[k].status == Status && Status != MAPS) {
			B = { boutons[k].x, boutons[k].y, boutons[k].w, boutons[k].h };
			SDL_RenderCopy(rendu, boutons[k].imagePress, NULL, &B); // pour afficher le bouton bien préssé
		}
	}

}


void affichageZoneEcriture(SDL_Renderer* rendu, SDL_Window* win, char text[])
{
	bool Running = true;
	SDL_Event ev;

	SDL_StartTextInput();
	while (Running)
	{
		while (SDL_PollEvent(&ev) != 0)
		{
			if (ev.type == SDL_QUIT)
				Running = false;
			else if (ev.type == SDL_TEXTINPUT || ev.type == SDL_KEYDOWN)
			{
				system("cls");
				if (ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_BACKSPACE && strlen(text) > 0)
				{
					affichageBouton(rendu);
					text[strlen(text) - 1] = '\0';
				}
				else if (ev.type == SDL_TEXTINPUT)
					strcat_s(text, 49, ev.text.text);

				TTF_Init();
				TTF_Font* font = TTF_OpenFont("C:\\Windows\\Fonts\\calibri.ttf", 25);
				SDL_Color couleur = { 0, 0,0 };
				SDL_Texture* texteRecu = loadText(rendu, text, couleur, font);
				SDL_Rect texte;
				texte.x = WZONEGAUCHE + BOUTON + 100;
				texte.y = YZONEJOUEUR + 20;
				texte.w = WZONEDROITE - (2 * BOUTON);
				texte.h = BOUTON;
				SDL_QueryTexture(texteRecu, NULL, NULL, &texte.w, &texte.h);
				SDL_RenderCopy(rendu, texteRecu, NULL, &texte);
				SDL_RenderPresent(rendu);
				SDL_DestroyTexture(texteRecu);
				TTF_CloseFont(font);
				if (ev.key.keysym.sym == SDLK_RETURN)
				{
					Running = false;
				}
			}
		}
		SDL_UpdateWindowSurface(win);
	}
	SDL_StopTextInput();
}

void affichageZoneAffichage(SDL_Renderer* rendu)
{
	TTF_Font* font = TTF_OpenFont("C:\\Windows\\Fonts\\calibri.ttf", 30);
	SDL_Color couleur = { 0, 0,0 };
	char instruction[10];
	char passage[5];

	SDL_Texture* texteRecu = loadText(rendu, "Deplacement :", couleur, font);
	SDL_Rect dplcmt;
	dplcmt.x = WZONEGAUCHE + BOUTON + 40;
	dplcmt.y = 260;
	dplcmt.w = WZONEDROITE - (2 * BOUTON);
	dplcmt.h = BOUTON;
	SDL_QueryTexture(texteRecu, NULL, NULL, &dplcmt.w, &dplcmt.h);
	SDL_RenderCopy(rendu, texteRecu, NULL, &dplcmt);
	SDL_DestroyTexture(texteRecu);


	for (int i = 0; i < nbrInstructions; i++)
	{
		strcpy_s(instruction, 10, Historique[i].mesure);
		sprintf_s(passage, 5, "%d", Historique[i].valeur);
		strcat_s(instruction, 10, " ");
		strcat_s(instruction, 10, passage);


		SDL_Texture* texteRecu = loadText(rendu, instruction, couleur, font);
		SDL_Rect texte;
		texte.x = WZONEGAUCHE + BOUTON + 95;
		texte.y = 320 + i * 35;
		texte.w = WZONEDROITE - (2 * BOUTON);
		texte.h = BOUTON;
		SDL_QueryTexture(texteRecu, NULL, NULL, &texte.w, &texte.h);
		SDL_RenderCopy(rendu, texteRecu, NULL, &texte);
		SDL_DestroyTexture(texteRecu);
	}
	SDL_RenderPresent(rendu);
	TTF_CloseFont(font);
}

void affichageBarreDroite(SDL_Renderer* rendu) {

	if(StatusG == GAME){
		SDL_Rect zoneOutils;
		zoneOutils.x = WZONEGAUCHE;
		zoneOutils.y = 0;
		zoneOutils.w = WZONEDROITE;
		zoneOutils.h = 250;
		SDL_SetRenderDrawColor(rendu, 200, 200, 200, 255);
		SDL_RenderFillRect(rendu, &zoneOutils);

		SDL_Rect zoneAffichage;
		zoneAffichage.x = WZONEGAUCHE;
		zoneAffichage.y = 250;
		zoneAffichage.w = WZONEDROITE;
		zoneAffichage.h = HAUTEUR - zoneAffichage.y - BOUTON;
		SDL_SetRenderDrawColor(rendu, 150, 150, 150, 255);
		SDL_RenderFillRect(rendu, &zoneAffichage);
	}
	else if (StatusG == CREATION) {
		SDL_Texture* texture = loadImage(rendu, "SPRITES/Celest.png");

		SDL_Rect fond;
		fond.x = WZONEGAUCHE;
		fond.y = 0;
		fond.w = WZONEDROITE;
		fond.h = HAUTEUR;
		SDL_RenderCopy(rendu, texture, NULL, &fond);
		SDL_DestroyTexture(texture);
	}
}

void affichageMenuMaps(SDL_Renderer* rendu) {

	SDL_Rect Bouton = { -30, 160, 160, 85 };

	for (int k = 0; k < nbrMap; k++) {
		if (Bouton.x >= WZONEGAUCHE) {
			Bouton.y += 100;
			Bouton.x = -30;
		}

		Bouton.x += 200;
		SDL_SetRenderDrawColor(rendu, 140, 140, 150, 255);
		SDL_RenderFillRect(rendu, &Bouton);
		SDL_SetRenderDrawColor(rendu, 60, 60, 60, 255);
		SDL_RenderDrawRect(rendu, &Bouton);

		TTF_Init();
		TTF_Font* font = TTF_OpenFont("C:\\Windows\\Fonts\\calibri.ttf", 25);

		SDL_Color couleur = {0,0,0,255};
		SDL_Rect positionTexte = { Bouton.x + Bouton.w / 2 - strlen(TABmap[k].nomMap) * 7, Bouton.y + Bouton.h / 2 - 10, 200, 100 };
		SDL_Texture* texture = loadText(rendu, TABmap[k].nomMap, couleur, font);
		SDL_QueryTexture(texture, NULL, NULL, &positionTexte.w, &positionTexte.h);
		SDL_RenderCopy(rendu, texture, NULL, &positionTexte);

		SDL_DestroyTexture(texture);


		Bouton.x += 60;
	}
}

void affichageVictoire(SDL_Renderer* rendu)
{
	SDL_Texture* texture_victoire = loadImage(rendu, "SPRITES/victoire.png");

	SDL_Rect victoire;
	victoire.w = 532;
	victoire.h = 112;
	victoire.x = (LARGEUR - victoire.w) / 2;
	victoire.y = (HAUTEUR - victoire.h) / 2;
	SDL_RenderCopy(rendu, texture_victoire, NULL, &victoire);

	SDL_RenderPresent(rendu);
	SDL_DestroyTexture(texture_victoire);
}

void affichageMAP(SDL_Renderer* rendu, Map& m)
{

	affichageFond(rendu, m);
	affichageObstacle(rendu, m);
	affichageBarreDroite(rendu);
	affichageBouton(rendu);
	affichageLigne(m.ligne, rendu);
	affichagePoint(m.ligne, rendu);
	affichageDepart_Arriver(rendu, m);
	affichageZoneAffichage(rendu);
}

void affichageGeneral(SDL_Renderer* rendu, Map& m) {

	switch (StatusG){

	case MENU_PRINC:
		affichageFond(rendu, m);
		affichageBouton(rendu);
		break;
	case GAME:
		affichageMAP(rendu, m);
		break;
	case CREATION:
		affichageMAP(rendu, m);
		break;
	case CHOIX_MAPS:
		affichageFond(rendu, m);
		affichageMenuMaps(rendu);
		affichageBouton(rendu);
		break;
	}
	SDL_RenderPresent(rendu);
}


//retourne l'indice de la map cliqué
int cliqueMap_boutons(int x, int y) {

	SDL_Rect Bouton = { -30, 160, 160, 85 };

	for (int k = 0; k < nbrMap; k++) {
		if (Bouton.x >= WZONEGAUCHE) {
			Bouton.y += 100;
			Bouton.x = -30;
		}

		Bouton.x += 200;

		if (x >= Bouton.x && x <= Bouton.x + Bouton.w
			&& y >= Bouton.y && y <= Bouton.y + Bouton.h)
			return k;


		Bouton.x += 60;
	}
	return -1;
}



void historique(char dir[])
{
	char test[5];
	char test2[5];
	double nbr;
	int cpt = 0;
	int i = 0;
	while (i < 2) {
		test[i] = dir[i];
		i++;
	}
	test[i] = '\0';
	if (strcmp(test, "AV") == 0)
	{
		strcpy_s(Historique[nbrInstructions].mesure, 3, "AV");
	}
	if (strcmp(test, "TR") == 0)
	{
		strcpy_s(Historique[nbrInstructions].mesure, 3, "TR");
	}
	if (strcmp(test, "AV") == 0 || strcmp(test, "TR") == 0) {
		while (dir[cpt] != ' ' && dir[cpt] != '\0')
		{
			cpt++;
		}
		int j = 0;

		while (dir[cpt + 1] != '\0')
		{
			test2[j] = dir[cpt + 1];
			j++;
			cpt++;
		}
		test2[j] = '\0';
		nbr = atof(test2);
		Historique[nbrInstructions].valeur = nbr;
		nbrInstructions++;
	}
}



void suppr_historique(SDL_Renderer* rendu)
{
	strcpy_s(Historique[nbrInstructions - 1].mesure, 2, "");
	Historique[nbrInstructions - 1].valeur = 0;
	nbrInstructions -= 1;
	if (nbrInstructions < 0)
		nbrInstructions = 0;
	
	affichageZoneAffichage(rendu);
}

bool arrivee(Robot r, Map& m)
{
	if ((r.x - r.taille / 2 < m.ligne.arrivee.x - DEPARTARRIVEE / 2  
		&& r.x + r.taille > m.ligne.arrivee.x - DEPARTARRIVEE / 2
		|| r.x - r.taille / 2 > m.ligne.arrivee.x - DEPARTARRIVEE / 2 && r.x - r.taille / 2 < m.ligne.arrivee.x + DEPARTARRIVEE)
		&& (r.x - r.taille / 2 < m.ligne.arrivee.x - DEPARTARRIVEE / 2
			&& r.y + r.taille > m.ligne.arrivee.y - DEPARTARRIVEE / 2
			|| r.y - r.taille / 2 > m.ligne.arrivee.y - DEPARTARRIVEE / 2 && r.y - r.taille / 2 < m.ligne.arrivee.y + DEPARTARRIVEE)) {
		return true;
	}
	else
	{
		return false;
	}
}

bool deplacement_robot(Map& m, SDL_Renderer* rendu)
{
	robot.angle = 0;
	double tmpx = m.ligne.depart.x - DEPARTARRIVEE / 2;
	double tmpy = m.ligne.depart.y - DEPARTARRIVEE / 2;
	for (int i = 0; i < nbrInstructions; i++)
	{
		if (strcmp(Historique[i].mesure, "AV") == 0)
		{
			affichageMAP(rendu, m);
			SDL_Texture* imgrobot = loadImage(rendu, "SPRITES/robot.png");
			SDL_Rect test;
			test.w = DEPARTARRIVEE;
			test.h = DEPARTARRIVEE;
			test.x = cos(robot.angle) * ((Historique[i].valeur * 39.7953) - 19.89765) * 2.2 + tmpx;
			test.y = sin(robot.angle) * ((Historique[i].valeur * 39.7953) - 19.89765) * 2.2 + tmpy;
			
			
			tmpx = test.x;
			tmpy = test.y;
			SDL_RenderCopy(rendu, imgrobot, NULL, &test);
			SDL_RenderPresent(rendu);
			SDL_DestroyTexture(imgrobot);
		}
		if (strcmp(Historique[i].mesure, "TR") == 0)
		{
			robot.angle += Historique[i].valeur * (M_PI / 180);
		}
	}

	robot.x = tmpx;
	robot.y = tmpy;

	return arrivee(robot, m);
}



void MAJstatus() {
	//on mets à jour le Status général en fonction du Status

	if (Status == MENU)
		StatusG = MENU_PRINC;
	if (Status == MAPS)
		StatusG = CHOIX_MAPS;
	if (Status == GAME_AJOUT || Status == GAME_MODIF || Status == GAME_SUPPR
		|| Status == GAME_INSTRUCTIONS || Status == GAME_RAPPORTEUR
		|| Status == GAME_REGLE || Status == GAME_VALIDER || Status == GAME_ANNULER || Status == GAME_VALID)
		StatusG = GAME;
	if (Status == CREATION_RECT || Status == CREATION_CERCLE || Status == CREATION_MAP)
		StatusG = CREATION;
}

bool clique_boutons(int x, int y) {

	for (int k = 0; k < nbrBoutons; k++) {

		if (x >= boutons[k].x && x <= boutons[k].x + boutons[k].w
			&& y >= boutons[k].y && y <= boutons[k].y + boutons[k].h) {

			switch (StatusG) {
			case MENU_PRINC:
				if (boutons[k].status == MAPS || boutons[k].status == CREATION_MAP)
					Status = boutons[k].status;
				break;
			case CREATION:
				if (boutons[k].status == CREATION_RECT || boutons[k].status == CREATION_CERCLE || boutons[k].status == CREATION_SAVE || boutons[k].status == RETOUR)
					Status = boutons[k].status;
				break;
			case CHOIX_MAPS:
				if (boutons[k].status == RETOUR)
					Status = boutons[k].status;
				break;
			case GAME:
				if (boutons[k].status == GAME_AJOUT || boutons[k].status == GAME_MODIF
					|| boutons[k].status == GAME_SUPPR || boutons[k].status == GAME_INSTRUCTIONS || boutons[k].status == GAME_RAPPORTEUR
					|| boutons[k].status == GAME_REGLE || boutons[k].status == GAME_VALIDER || boutons[k].status == GAME_ANNULER || boutons[k].status == RETOUR || boutons[k].status == GAME_VALID)
					Status = boutons[k].status;
				break;
			}
			MAJstatus();
			return true;
		}
	}


	return false;
}




// il va s'agir ici de creer et de recup une sauvegarde


void RECUPsave(const char nom_fichier[]) {
	//utilisation de TABmap (variables global)

	ifstream entree(nom_fichier, ios::in);
	if (!entree)
		cout << "Probleme d'ouverture de la sauvegarde \n";
	else {
		entree >> nbrMap;

		for (int j = 0; j < nbrMap; j++) {
			entree >> TABmap[j].nomMap;
			entree >> TABmap[j].fond;

			entree >> TABmap[j].ligne.depart.x;
			entree >> TABmap[j].ligne.depart.y;


			entree >> TABmap[j].ligne.arrivee.x;
			entree >> TABmap[j].ligne.arrivee.y;

			init_ligne(TABmap[j].ligne, TABmap[j].ligne.depart.x, TABmap[j].ligne.depart.y, TABmap[j].ligne.arrivee.x, TABmap[j].ligne.arrivee.y, 1);

			entree >> TABmap[j].nbrObstacles;

			for (int k = 0; k < TABmap[j].nbrObstacles; k++) {
				int forme;
				entree >> forme;
				if (forme == 0)
					TABmap[j].TABobstacle[k].forme = RECTANGLE;

				if (forme == 1)
					TABmap[j].TABobstacle[k].forme = CERCLE;


				entree >> TABmap[j].TABobstacle[k].x;
				entree >> TABmap[j].TABobstacle[k].y;
				entree >> TABmap[j].TABobstacle[k].rayonX;
				entree >> TABmap[j].TABobstacle[k].rayonY;
				int r, g, b, a;
				entree >> r >> g >> b >> a;
				TABmap[j].TABobstacle[k].color.r = r;
				TABmap[j].TABobstacle[k].color.g = g;
				TABmap[j].TABobstacle[k].color.b = b;
				TABmap[j].TABobstacle[k].color.a = a;
			}
		}
	}
	entree.close();
}


void CREATEsave(const char nom_fichier[]) {

	ofstream sorti(nom_fichier, ios::trunc);

	sorti << nbrMap << endl << endl;

	for (int k = 0; k < nbrMap; k++) {

		sorti << TABmap[k].nomMap << endl;
		sorti << TABmap[k].fond << endl << endl;


		sorti << TABmap[k].ligne.depart.x << " " << TABmap[k].ligne.depart.y << endl;
		sorti << TABmap[k].ligne.arrivee.x << " " << TABmap[k].ligne.arrivee.y << endl;

		sorti << endl;

		sorti << TABmap[k].nbrObstacles << endl;

		for (int i = 0; i < TABmap[k].nbrObstacles; i++) {
			sorti << TABmap[k].TABobstacle[i].forme << endl;
			sorti << TABmap[k].TABobstacle[i].x << " " << TABmap[k].TABobstacle[i].y << endl;
			sorti << TABmap[k].TABobstacle[i].rayonX << " " << TABmap[k].TABobstacle[i].rayonY << endl;

			int r, g, b, a;
			r = TABmap[k].TABobstacle[i].color.r;
			g = TABmap[k].TABobstacle[i].color.g;
			b = TABmap[k].TABobstacle[i].color.b;
			a = TABmap[k].TABobstacle[i].color.a;

			sorti << r << " " << g << " " << b << " " << a << endl << endl;
		}
	}

	sorti.close();

}



void initialisation_general(SDL_Renderer* rendu) {

	RECUPsave("SAVE/save.txt");

	for (int k = 0; k < nbrMap; k++) {
		TABmap[k].ligne.nbrPoints = 1;
	}

	//initialisation des boutons

	init_bouton(boutons[0], rendu, WZONEGAUCHE + BOUTON, YZONEJOUEUR, WZONEDROITE - (2 * BOUTON), BOUTON, "ZoneEcriture", GAME_INSTRUCTIONS, " ", " ");
	init_bouton(boutons[1], rendu, WZONEGAUCHE + 20, 43.5, 100, BOUTON, "Ajouter", GAME_AJOUT, "SPRITES/Unpressed AJOUTER.png", "SPRITES/Pressed AJOUTER.png");
	init_bouton(boutons[2], rendu, WZONEGAUCHE + 2 * 20 + 100, 43.5, 100, BOUTON, "Modifier", GAME_MODIF, "SPRITES/Unpressed MODIFIER.png", "SPRITES/Pressed MODIFIER.png");
	init_bouton(boutons[3], rendu, WZONEGAUCHE + 3 * 20 + 200, 43.5, 100, BOUTON, "Supprim", GAME_SUPPR, "SPRITES/Unpressed SUPPRIMER.png", "SPRITES/Pressed SUPPRIMER.png");
	init_bouton(boutons[4], rendu, WZONEGAUCHE + 80, 43.5 + 43 + BOUTON, 100, BOUTON, "Regle", GAME_REGLE, "SPRITES/Unpressed REGLE.png", "SPRITES/Pressed REGLE.png");
	init_bouton(boutons[5], rendu, WZONEGAUCHE + 80 + 120, 43.5 + 43 + BOUTON, 100, BOUTON, "Rapport", GAME_RAPPORTEUR, "SPRITES/Unpressed RAPPORTEUR.png", "SPRITES/Pressed RAPPORTEUR.png");

	init_bouton(boutons[6], rendu, (LARGEUR / 2) - 90 - 640 / 2, HAUTEUR - 235, 640 / 2, 320 / 2, "CREER_MAP",CREATION_MAP, "SPRITES/CreationMap.png", "SPRITES/CreationMap.png");
	init_bouton(boutons[7], rendu, (LARGEUR / 2) + 90, HAUTEUR - 235, 640 / 2, 320 / 2, "JOUER", MAPS, "SPRITES/SelectionMap.png", "SPRITES/SelectionMap.png");
	
	init_bouton(boutons[8], rendu, LARGEUR - WZONEDROITE / 2 - 72, HAUTEUR / 2 + 20, 114 * 2, 39 * 2, "Cercle", CREATION_CERCLE, "SPRITES/Unpressed CERCLE.png", "SPRITES/Pressed CERCLE.png");
	init_bouton(boutons[9], rendu, LARGEUR - WZONEDROITE / 2 - 72, HAUTEUR / 2 - 30 - 39, 114 * 2, 39 * 2, "Rectangle", CREATION_RECT, "SPRITES/Unpressed RECTANGLE.png", "SPRITES/Pressed RECTANGLE.png");
	
	init_bouton(boutons[10], rendu, LARGEUR - BOUTON, YZONEJOUEUR, BOUTON, BOUTON, "Valider", GAME_VALIDER, "SPRITES/boutonValider.png", "SPRITES/boutonValider.png");
	init_bouton(boutons[11], rendu, WZONEGAUCHE, YZONEJOUEUR, BOUTON, BOUTON, "Annuler", GAME_ANNULER, "SPRITES/boutonAnnuler.png", "SPRITES/boutonAnnuler.png");
	
	init_bouton(boutons[12], rendu, 30, 30, 50, 50, "RETOUR", RETOUR, "SPRITES/retour.png", "SPRITES/retour.png");
	init_bouton(boutons[13], rendu, 100, 30, 50, 50, "SAVE", CREATION_SAVE, "SPRITES/Save.png", "SPRITES/Save.png");
	
	init_bouton(boutons[14], rendu, 100, 30, 100, 60, "VALID", GAME_VALID, "SPRITES/Validation.png", "SPRITES/Validation.png");


	nbrBoutons = 15;

}



void destruct_bouton()
{
	for (int i = 0; i < nbrBoutons; i++)
	{
		SDL_DestroyTexture(boutons[i].imagePress);
		SDL_DestroyTexture(boutons[i].imageUnPress);
	}
}



double calculAngle(int x, int y, int xSouris, int ySouris) {

	double angle = 0;

	if (ySouris - y != 0)
		angle = acos((sqrt((x - xSouris) * (x - xSouris) + (y - y) * (y - y))) / (sqrt((y - ySouris) * (y - ySouris) + (x - xSouris) * (x - xSouris)))) * (180 / M_PI);

	if (-ySouris + y < 0)
		angle -= 180;

	if (-ySouris + y < 0 && xSouris - x < 0
		|| -ySouris + y > 0 && xSouris - x > 0) {
		angle *= -1;
		angle -= 180;
	}

	angle -= 90;

	return angle;

}






/*=======================================
				Main
=========================================*/

int main(int argn, char* argv[]) {
	_CrtMemState sOld; // snapshot de mémoire
	_CrtMemState sNew;
	_CrtMemState sDiff;
	_CrtMemCheckpoint(&sOld); // initialiser le premier


	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		cout << "Echec à l’ouverture";
		return 1;
	}

	//on crée la fenêtre
	SDL_Window* win = SDL_CreateWindow("Projet - Groupe 3-2",
		SDL_WINDOWPOS_CENTERED,     //pos. X: autre option: SDL_WINDOWPOS_UNDEFINED
		SDL_WINDOWPOS_CENTERED,     //pos. Y: autre option: SDL_WINDOWPOS_UNDEFINED 
		LARGEUR, 			//largeur en pixels			
		HAUTEUR, 			//hauteur en pixels
		SDL_WINDOW_SHOWN //d’autres options (plein ecran, resizable, sans bordure...)
	);
	if (win == NULL)
		cout << "erreur ouverture fenetre";

	//Création d’un dessin associé à la fenêtre (1 seul renderer par fenetre)
	SDL_Renderer* rendu = SDL_CreateRenderer(
		win,  //nom de la fenêtre
		-1, //par défaut
		SDL_RENDERER_ACCELERATED); //utilisation du GPU, valeur recommandée




	initialisation_general(rendu);

	Map m = TABmap[nbrMap - 1];

	affichageGeneral(rendu, m);


	bool continuer = true;   //booléen fin de programme
	SDL_Event event;


	while (continuer)
	{
		affichageGeneral(rendu, m);

		robot.angle = 0;
		robot.taille = 30;
		robot.x = 45;
		robot.y = 675;


		SDL_WaitEvent(&event);
		switch (event.type)
		{
		case SDL_QUIT:
			continuer = false;
			break;
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE)
			{
				continuer = false;
				break;
			}
		}


		char dir[50] = ""; // pour GAME_INSTRUCTION
		int indice = -1; // pour GAME_MODIF
		SDL_Rect rect; // pour CREATION_RECTANGLE
		int x = event.button.x, y = event.button.y; // pour GAME_REGLE et RAPPORTEUR
		bool arrive;

		switch (Status)
		{
		case GAME_AJOUT:
			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
			{
				clique_boutons(event.button.x, event.button.y);
				
				if (event.button.x < WZONEGAUCHE && Status == GAME_AJOUT) {
					ajoute_point(m.ligne, event.button.x, event.button.y);
				}
				
			}
			break;
		case GAME_MODIF:

			indice = clique_points(m.ligne, event.button.x, event.button.y);

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT && indice != -1 && indice != 0) {

				while (event.type != SDL_MOUSEBUTTONUP) {

					if (event.button.x < WZONEGAUCHE) {

						m.ligne.ligne[indice].x = event.button.x;
						m.ligne.ligne[indice].y = event.button.y;

						affichageGeneral(rendu, m);

					}
					SDL_PollEvent(&event);
				}
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				clique_boutons(event.button.x, event.button.y);

			break;
		case GAME_SUPPR:
			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
			{
				if (event.button.x < WZONEGAUCHE) {
					supprime_point(m.ligne, event.button.x, event.button.y);
				}


				clique_boutons(event.button.x, event.button.y);

			}
			break;
		case GAME_INSTRUCTIONS:

			affichageZoneEcriture(rendu, win, dir);
			affichageBouton(rendu);
			historique(dir);

			Status = GAME_AJOUT;
			break;
		case GAME_RAPPORTEUR:

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				clique_boutons(event.button.x, event.button.y);
			if (event.button.x < WZONEGAUCHE)
			{
				if (event.button.x != x || event.button.y != y)
					affichageGeneral(rendu, m);

				x = event.button.x;
				y = event.button.y;


				SDL_Texture* rapporteur = loadImage(rendu, "SPRITES/rapporteur.png");
				SDL_Rect zoneRapporteur;
				zoneRapporteur.w = 500;
				zoneRapporteur.h = 500;
				zoneRapporteur.x = event.button.x - zoneRapporteur.w / 2;
				zoneRapporteur.y = event.button.y - zoneRapporteur.h / 2;

				const SDL_RendererFlip flip = SDL_FLIP_NONE;
				SDL_RenderCopyEx(rendu, rapporteur, NULL, &zoneRapporteur, 360, NULL, flip);
				SDL_RenderPresent(rendu);


				if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT && event.button.x < WZONEGAUCHE) {
					double angle = 0;
					while (event.type != SDL_MOUSEBUTTONUP) {

						if (event.button.x != x || event.button.y != y)
							affichageGeneral(rendu, m);

						angle = calculAngle(x, y, event.button.x, event.button.y);

						SDL_RenderCopyEx(rendu, rapporteur, NULL, &zoneRapporteur, angle, NULL, flip);
						SDL_RenderPresent(rendu);

						SDL_PollEvent(&event);
					}
				}
				SDL_DestroyTexture(rapporteur);
			}
			break;
		case GAME_REGLE:
			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				clique_boutons(event.button.x, event.button.y);

			if (event.button.x < WZONEGAUCHE)
			{
				if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {

					const int x = event.button.x;
					const int y = event.button.y;
					SDL_Point center = { 0,0 };
					SDL_Texture* regle = loadImage(rendu, "SPRITES/regle.png");

					SDL_Rect zoneRegle;
					zoneRegle.w = 866 * 3;
					zoneRegle.h = 110;
					zoneRegle.x = event.button.x;
					zoneRegle.y = event.button.y;

					while (event.type != SDL_MOUSEBUTTONUP) {
						if (event.button.x != x || event.button.y != y)
							affichageGeneral(rendu, m);

						double hypot = sqrt((y - event.button.y) * (y - event.button.y) + (x - event.button.x) * (x - event.button.x));
						double adja = sqrt((x - event.button.x) * (x - event.button.x) + (y - y) * (y - y));

						double angle = acosf((adja * 1.0 / hypot * 1.0)) * 180.0 / M_PI;
						if (event.button.x < x && event.button.y < y)  // haut gauche X
							angle = (acosf((adja * 1.0 / hypot * 1.0)) * 180.0 / M_PI) + 180;

						else if (event.button.x > x && event.button.y > y)  // bas droite X
							angle = acosf((adja * 1.0 / hypot * 1.0)) * 180.0 / M_PI;

						else if (event.button.x < x && event.button.y > y)  // bas gauche X
							angle = 180 - acosf((adja * 1.0 / hypot * 1.0)) * 180.0 / M_PI;

						else if (event.button.x > x && event.button.y < y)  // haut droite
							angle = -1 * acosf((adja * 1.0 / hypot * 1.0)) * 180.0 / M_PI;


						const SDL_RendererFlip flip = SDL_FLIP_NONE;
						SDL_RenderCopyEx(rendu, regle, NULL, &zoneRegle, angle, &center, flip);
						SDL_RenderPresent(rendu);

						SDL_PollEvent(&event);
					}

					SDL_DestroyTexture(regle);
				}
			}

			break;
		case GAME_ANNULER:

			suppr_historique(rendu);
			if (nbrInstructions < 0)
				nbrInstructions == 0;
			Status = GAME_AJOUT;
			break;
		case GAME_VALIDER:

			arrive = deplacement_robot(m, rendu);
			SDL_Delay(2000);

			if (arrive) {
				affichageVictoire(rendu);
				SDL_Delay(2000);
				Status = RETOUR;
			}
			else {
				Status = GAME_AJOUT;
			}
			break;
		case GAME_VALID:
			verif_ligne(rendu, m, robot);

			SDL_Delay(4000);

			Status = GAME_AJOUT;

			break;
		case MENU:

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				clique_boutons(event.button.x, event.button.y);

			break;

		case CREATION_RECT:

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT && event.button.x < WZONEGAUCHE) {
				rect.x = event.button.x;
				rect.y = event.button.y;
				rect.w = 0;
				rect.h = 0;

				while (event.type != SDL_MOUSEBUTTONUP) {

					if (event.button.x < WZONEGAUCHE) {
						if (rect.x != event.button.x && rect.y != event.button.y)
							affichageGeneral(rendu, m);

						rect.w = event.button.x - rect.x;
						rect.h = event.button.y - rect.y;

						SDL_RenderFillRect(rendu, &rect);
						SDL_RenderPresent(rendu);

					}
					SDL_PollEvent(&event);
				}

				ajoute_obstacle(m, RECTANGLE, rect.x, rect.y, rect.w, rect.h, { 255, 30, 75, 255 });
				clique_boutons(event.button.x, event.button.y);
			}

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				clique_boutons(event.button.x, event.button.y);

			break;
		case CREATION_CERCLE:

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT && event.button.x < WZONEGAUCHE)
			{
				int x = event.button.x;
				int y = event.button.y;
				double hypot;

				while (event.type != SDL_MOUSEBUTTONUP)
				{
					if (event.button.x < WZONEGAUCHE)
					{
						if (x != event.button.x && y != event.button.y)
							affichageGeneral(rendu, m);
						hypot = sqrt((y - event.button.y) * (y - event.button.y) + (x - event.button.x) * (x - event.button.x));

						for (double dy = 1; dy <= hypot; dy += 1.0)
						{
							double dx = floor(sqrt((2.0 * hypot * dy) - (dy * dy)));
							SDL_RenderDrawLine(rendu, x - dx, y + dy - hypot, x + dx, y + dy - hypot);
							SDL_RenderDrawLine(rendu, x - dx, y - dy + hypot, x + dx, y - dy + hypot);
						}
						SDL_RenderPresent(rendu);
					}
					SDL_PollEvent(&event);
				}
				ajoute_obstacle(m, CERCLE, x, y, hypot, hypot, { 255, 30, 75, 255 });
				clique_boutons(event.button.x, event.button.y);
			}
			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
				clique_boutons(event.button.x, event.button.y);
			break;
		case CREATION_MAP:

			nbrMap++;
			m = TABmap[nbrMap - 1];
			init_ligne(m.ligne, 45, 675, 855, 45, 1);
			init_map(m, "map", "NONE", m.ligne, m.TABobstacle, 0);
			Status = CREATION_RECT;
			StatusG = CREATION;

			break;
		case CREATION_SAVE:


			Status = MENU;
			StatusG = MENU_PRINC;
			init_map(TABmap[nbrMap - 1], "map", "NONE", m.ligne, m.TABobstacle, m.nbrObstacles);

			CREATEsave("SAVE/save.txt");

			break;

		case MAPS:

			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
				int k = cliqueMap_boutons(event.button.x, event.button.y);
				if (k != -1) {
					m = TABmap[k];
					Status = GAME_AJOUT;
					MAJstatus();
				}


				clique_boutons(event.button.x, event.button.y);
			}
			break;

		case RETOUR:

			switch (StatusG) {
			case GAME:
				Status = MAPS;
				StatusG = CHOIX_MAPS;
				break;
			case CREATION:
				nbrMap--;
				m = TABmap[nbrMap - 1];
				Status = MENU;
				StatusG = MENU_PRINC;
				break;
			case CHOIX_MAPS:
				Status = MENU;
				StatusG = MENU_PRINC;
				break;
			}
			break;
		}
	}
	//destruction du renderer à la fin
	SDL_DestroyRenderer(rendu);

	//destruction à la fin
	SDL_DestroyWindow(win);   //equivalent du delete

	//fermeture
	SDL_Quit();



	_CrtMemCheckpoint(&sNew); // initialiser le dernier
	_CrtMemDifference(&sDiff, &sOld, &sNew); // créer la diff entre les deux états de mémoire
	OutputDebugString(L"-----------_CrtMemDumpStatistics ---------"); // bilan
	_CrtMemDumpStatistics(&sDiff);
	OutputDebugString(L"-----------_CrtMemDumpAllObjectsSince ---------"); // dump les objets pas supprimé
	_CrtMemDumpAllObjectsSince(&sOld);
	OutputDebugString(L"-----------_CrtDumpMemoryLeaks ---------"); // te dis s'il y a eu fuite mémoire
	_CrtDumpMemoryLeaks(); // note: avec la fonction avant, la fuite est gérée mais on te le signal quand même
	return 0; // lancer avec un point d'arrête sur le return et en mode debugger
}