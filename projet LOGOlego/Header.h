//Les constantes pour la SDL
const int LARGEUR = 1280;
const int HAUTEUR = 720;
const int BOUTON = 60;
const int DEPARTARRIVEE = 30;
const int YZONEJOUEUR = HAUTEUR - BOUTON;
const int WZONEDROITE = 380;
const int WZONEGAUCHE = LARGEUR - WZONEDROITE;
const int TaillePoint = 7.5 * 2;

//Un type Forme composé de rectangle et de cercle
enum Forme { RECTANGLE, CERCLE };

//Status de l'utilisateur (en jeu, en menu de jeu, sur le menu principal, ...)
enum STATUS {
	GAME_AJOUT,
	GAME_MODIF,
	GAME_SUPPR,
	GAME_INSTRUCTIONS,
	GAME_RAPPORTEUR,
	GAME_REGLE,
	GAME_VALIDER,
	GAME_ANNULER,
	GAME_VALID,


	MENU,
	MAPS,

	CREATION_MAP,
	CREATION_RECT,
	CREATION_CERCLE,
	CREATION_SAVE,


	RETOUR
};

enum STATUS_GENERAL {
	GAME,
	MENU_PRINC,
	CREATION,
	CHOIX_MAPS
};


//Une structure Point pour les coordonnées d'un point
struct Point {
	int x; //Abscisse
	int y; //Ordonnée
};

//Une structure Ligne pour la ligne complète du point de départ au point d'arrivée
struct Ligne
{
	Point depart; //Structure Point
	Point arrivee; //Structure Point
	Point ligne[100]; //Liste de tous les points
	int nbrPoints; //Nombre de point actuel sur la ligne
};

//Une structure Obstacle pour la forme, la position et la taille d'une forme
struct Obstacle 
{
	Forme forme; //Structure Forme
	int x; //Abscisse
	int y; //Ordonnée
	int rayonX; //Taille du rayon x
	int rayonY; //Taille du rayon y
	SDL_Color color;
};

//Une structure Map pour décrire une map
struct Map {
	char nomMap[50]; //Nom de la map
	Ligne ligne; //Structure Ligne
	Obstacle TABobstacle[150]; //Structure Obstacle
	int nbrObstacles; //Nombre d'obstacle sur la map
	char fond[10];

};



struct Instruction {


	char mesure[3]; // AV : avance, TR : tourne
	int valeur; // d'angle ou de distance

};


struct Bouton {

	int x;
	int y;
	int w;
	int h;

	char nom[50];
	STATUS status;
	SDL_Texture *imagePress;
	SDL_Texture *imageUnPress;
};

STATUS Status =  MENU;

STATUS_GENERAL StatusG = MENU_PRINC;

Map TABmap[100]; // toute les maps
int nbrMap; //nb maps

Instruction Historique[100]; //liste des instructions que l'utilisateur rentre;
int nbrInstructions = 0; // nombre d'instructions actuelement donné

Bouton boutons[50];
int nbrBoutons;





//Une structure Robot pour décrire le robot
struct Robot
{
	int x; //Abscisse (milieu du robot)
	int y; //Ordonnée
	int taille; //Taille du robot
	float angle; //Angle de déplacement
	SDL_Color color; // couleur de l'objet
};

Robot robot;